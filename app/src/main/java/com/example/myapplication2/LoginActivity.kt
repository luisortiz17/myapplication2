package com.example.myapplication2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.example.myapplication2.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity(), Validator.ValidationListener{

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Validaciones
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)

        binding.setClickListener {

            when (it!!.id) {
                binding.btnLogin.id -> {
                    //validator!!.toValidate()
                    intent = Intent(applicationContext, MenuActivity::class.java)
                    startActivity(intent)

                }

                binding.btnRegister.id ->{
                    intent = Intent(applicationContext, RegistroActivity::class.java)
                    startActivity(intent)
                }

                binding.recuperaPwd.id ->{
                    intent = Intent(applicationContext, RecuperarPasswordActivity::class.java)
                    startActivity(intent)
                }
            }}


    }

    override fun onValidationSuccess() {
        Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@LoginActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }


}