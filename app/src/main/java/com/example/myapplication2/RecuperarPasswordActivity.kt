package com.example.myapplication2

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.example.myapplication2.databinding.ActivityRecuperarPasswordBinding

class RecuperarPasswordActivity: ToolbarActivity(), Validator.ValidationListener {

    private val binding by lazy{
        DataBindingUtil.setContentView<ActivityRecuperarPasswordBinding>(this, R.layout.activity_recuperar_password)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inicializarToolbar(binding.barraP, "OLVIDE CONTRASEÑA", 1)

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnConfirmar.id -> {
                    validator!!.toValidate()
                }
            }
        }


    }

    override fun onValidationSuccess() {
        Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@RecuperarPasswordActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}