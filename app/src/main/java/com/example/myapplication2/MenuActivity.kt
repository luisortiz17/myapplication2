package com.example.myapplication2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.example.myapplication2.databinding.ActivityMenuBinding
import com.example.myapplication2.databinding.ActivityRegistroBinding

class MenuActivity : AppCompatActivity() {

    private val binding by lazy{
        DataBindingUtil.setContentView<ActivityMenuBinding>(this, R.layout.activity_menu)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.setClickListener {

            when (it!!.id) {


                binding.btnCheckIn.id ->{
                    intent = Intent(applicationContext, FirmaActivity::class.java)
                    startActivity(intent)
                }

                binding.btnProfile.id ->{
                    intent = Intent(applicationContext, ProfileActivity::class.java)
                    startActivity(intent)
                }
            }}


    }
}